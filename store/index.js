import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = () =>
	new Vuex.Store({
		state: {
			counter: 0
		},
		mutations: {
			nuxtState(state, data) {
				state.counter++;
			}
		},
		actions: {
			nuxtBrowser({ commit }, data) {
				commit('nuxtState', data);
			}
		}
	});

export default store;
