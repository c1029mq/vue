import { Card, XButton, Divider, Scroller, ToastPlugin } from 'vux';
import Vue from 'vue';
Vue.use(ToastPlugin);
Vue.component('Divider', Divider);
Vue.component('XButton', XButton);
Vue.component('Scroller', Scroller);
Vue.component('Card', Card);
