import Vue from 'vue';
var comsys = {
	install(Vue) {
		Vue.prototype.comsys = {
			val: function(val) {
				console.log(val);
				return val;
			}
		};
	}
};
function setHtmlFontSize(designWidth, maxWidth) {
	var htmlEl = document.body.parentNode || document.documentElement;
	var momentWith = 0;
	momentWith = htmlEl.clientWidth > maxWidth ? maxWidth : htmlEl.clientWidth;
	htmlEl.style.fontSize = momentWith / designWidth * 100 + 'px';
	window.addEventListener(
		'resize',
		function() {
			setHtmlFontSize(designWidth, maxWidth);
		},
		false
	);
}
Vue.mixin({
	created() {
		if (this.$store) {
			setHtmlFontSize(1080, 1080);
		}
	}
});
Vue.use(comsys);
