export default function(context) {
	context.store.dispatch('nuxtBrowser');
	console.log(context, 'context');
	context.userAgent = process.server ? context.req.headers['user-agent'] : navigator.userAgent;
}
